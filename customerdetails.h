#ifndef CUSTOMERDETAILS_H
#define CUSTOMERDETAILS_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QDebug>
#include "qbear.h"

namespace Ui {
class customerDetails;
}

enum class customertablename {
    ID,NAME,FIRSTNAME,ADDRESS1,ADDRESS2,PHONE,MOBILE,EMAIL,BIRTHDAY,NOTE,NATIONALITY,CITY,CIVILITY
};

class customerDetails : public QDialog
{
    Q_OBJECT

public:
    explicit customerDetails(QWidget *parent = 0);
    ~customerDetails();

    QModelIndex getIndex() const;
    void setIndex(const QModelIndex &value);

    QVariant getData(customertablename tag);

private slots:
    void on_buttonBox_accepted();

    void on_comboBox_country_currentIndexChanged(int index);

signals:
    void completed();

private:
    Ui::customerDetails *ui;
    QModelIndex index;
    QPointer<QBear> qbear;
    QSqlDatabase db;
    QPointer<QSqlQueryModel> model_invoice_client;
};

#endif // CUSTOMERDETAILS_H
