#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QDialog>
#include <QtSql>
#include <QPointer>
#include <QDebug>
#include <QSqlQuery>
#include <QMessageBox>

namespace Ui {
class Customer;
}

class Customer : public QDialog
{
    Q_OBJECT
    
public:
    explicit Customer(QWidget *parent = 0);
    ~Customer();

    void deleteCustomer();
    
private slots:

    void on_pushButton_clicked();

    void on_removeButton_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_pushButton_3_clicked();

public slots:
    void updateTableView();

signals:
    void changeOccured();

private:
    Ui::Customer *ui;
    QPointer<QSqlTableModel> model;
    QSqlDatabase db;
};

#endif // CUSTOMER_H
