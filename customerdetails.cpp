#include "customerdetails.h"
#include "ui_customerdetails.h"

customerDetails::customerDetails(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::customerDetails)
{
    ui->setupUi(this);
    qbear = new QBear();
    db = QSqlDatabase::database("ouatelse");
}

customerDetails::~customerDetails()
{
    delete ui;
}
QModelIndex customerDetails::getIndex() const
{
    return index;
}

void customerDetails::setIndex(const QModelIndex &value)
{
    index = value;
    ui->lineEdit_code->setText(getData(customertablename::ID).toString());
    ui->lineEdit_name->setText(getData(customertablename::NAME).toString());
    ui->lineEdit_firstname->setText(getData(customertablename::FIRSTNAME).toString());
    ui->lineEdit_address1->setText(getData(customertablename::ADDRESS1).toString());
    ui->lineEdit_address2->setText(getData(customertablename::ADDRESS2).toString());
    ui->lineEdit_email->setText(getData(customertablename::EMAIL).toString());
    ui->lineEdit_phone->setText(getData(customertablename::PHONE).toString());
    ui->lineEdit_mobile->setText(getData(customertablename::MOBILE).toString());
    ui->kdatecombobox_birthday->setDate(getData(customertablename::BIRTHDAY).toDate());
    ui->textEdit_note->setText(getData(customertablename::NOTE).toString());
    ui->comboBox_civility->setCurrentIndex(getData(customertablename::CIVILITY).toInt());

    qbear->fillComboboxCountry(ui->comboBox_country);
    ui->comboBox_country->setCurrentIndex(ui->comboBox_country->findData(getData(customertablename::NATIONALITY).toInt()));

    qbear->fillComboboxCity(ui->comboBox_city, ui->comboBox_country->itemData(ui->comboBox_country->currentIndex()).toInt());
    ui->comboBox_city->setCurrentIndex(ui->comboBox_city->findData(getData(customertablename::CITY).toInt()));

    ui->spinBox_buy_frequency->setValue(qbear->getBuyFrequency(getData(customertablename::ID).toInt()));
    ui->spinBox_quantity_month->setValue(qbear->getQuantityPerMonth(getData(customertablename::ID).toInt()));
    
    model_invoice_client = new QSqlQueryModel();
    model_invoice_client->setQuery(QString("SELECT date, moyens_paiements_id, pourcentage_remise, salaries_id "
                                           "FROM oua_facture "
                                           "WHERE clients_id = %1").arg(ui->lineEdit_code->text()),db);
    ui->tableView_invoice->setModel(model_invoice_client);
}

QVariant customerDetails::getData(customertablename tag)
{
    return index.sibling(index.row(),(int)tag).data();
}

void customerDetails::on_buttonBox_accepted()
{
    if (db.isOpen()) {
        QSqlQuery q(db);
        q.prepare("UPDATE oua_client SET nom=:name, prenom=:firstname, adresse1=:adresseFirst, adresse2=:adresseSecond, fixe=:phone, portable=:mobile, mail=:mail, naissance=:birthday, notes=:notes, nationalite_id=:idNation, villes_id=:idCity, civilite_id=:idCivil WHERE id=:id");
        q.bindValue(":id", ui->lineEdit_code->text());
        q.bindValue(":name", ui->lineEdit_name->text());
        q.bindValue(":firstname", ui->lineEdit_firstname->text());
        q.bindValue(":adresseFirst", ui->lineEdit_address1->text());
        q.bindValue(":adresseSecond", ui->lineEdit_address2->text());
        q.bindValue(":phone", ui->lineEdit_phone->text());
        q.bindValue(":mobile", ui->lineEdit_mobile->text());
        q.bindValue(":mail", ui->lineEdit_email->text());
        q.bindValue(":birthday", ui->kdatecombobox_birthday->date());
        q.bindValue(":notes", ui->textEdit_note->toPlainText());
        q.bindValue(":idNation", ui->comboBox_country->itemData(ui->comboBox_country->currentIndex()));
        q.bindValue(":idCity", ui->comboBox_city->itemData(ui->comboBox_city->currentIndex()));
        q.bindValue(":idCivil", ui->comboBox_civility->currentIndex());
        q.exec();
    }
    emit completed();
}

void customerDetails::on_comboBox_country_currentIndexChanged(int index)
{
    qbear->fillComboboxCity(ui->comboBox_city, ui->comboBox_country->itemData(index).toInt());
}
