#include "login.h"
#include "ui_login.h"
#include <QDebug>
#include <QTextStream>
#include <QPointer>

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
}

Login::~Login()
{
    delete ui;
}

void Login::on_buttonBox_accepted(){

    bool connect = false;
    int id = -1;

    QSqlDatabase db = QSqlDatabase::database("ouatelse");

    if (db.isOpen()) {

        QSqlQuery query("Select login, password, id, roles_id from oua_salarie",db);

        while (query.next() && !connect){
            QString login = query.value((int)logintable::LOGIN).toString();
            QString password = query.value((int)logintable::PASSWORD).toString();
            int id = query.value((int)logintable::ID).toInt();
            int role = query.value((int)logintable::ROLE).toInt();

            if (login == ui->lineEdit_pwd->text() && password == ui->lineEdit_pwd->text()) {
                if (role == (int)roletable::CEO) {
                    emit isCEO(id);
                }
                else if (role == (int)roletable::DEALER) {
                    emit isDealer(id);
                }
                connect = true;
            } else {
                connect = false;
            }
        }
    }

    if (connect) {
        emit connected();
    } else {
        QMessageBox::warning(this, tr("Permission denied"), tr("Possibly incorrect password, please try again."), QMessageBox::Ok);
        this->show();
        this->raise();
    }
}

