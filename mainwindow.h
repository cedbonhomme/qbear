#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QPointer>
#include <QStandardItemModel>
#include <QAbstractButton>
#include "qbear.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_actionLogin_triggered();

    void on_actionView_Customers_triggered();

    void on_actionView_Products_triggered();

    void on_actionView_Invoices_triggered();

    void on_actionDisconnect_triggered();

    void on_actionAbout_QBear_triggered();

    void on_actionSettings_triggered();

    void on_listWidget_currentRowChanged(int currentRow);

    void on_pushButton_add_customer_clicked();

    void on_buttonBox_accepted();

    void on_pushButton_add_product_to_checkout_clicked();

    void on_actionView_Employee_triggered();

    void on_spinBox_sale_valueChanged(int arg1);

    void on_buttonBox_rejected();

    void on_buttonBox_clicked(QAbstractButton *button);

    void on_toolButton_clicked();

public slots:
    void updateCustomerList();
    void updateProductList();
    void updateEmployeeList();
    void updateInvoiceList();
    void onSuccessfulLogin();
    void onPeasantDealerConnect(int idDealer);
    void onGloriousCEOConnect(int idCEO);

private:
    Ui::MainWindow *ui;
    QSettings settings;
    QSqlDatabase db;
    QPointer<QBear> qbear;
    QPointer<QStandardItemModel> checkout_model;
    double total_product;
    QStringList checkout_model_header;
    int employeeId;
};

#endif // MAINWINDOW_H
