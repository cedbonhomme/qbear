#include <QApplication>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QtDebug>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Dark Honey");
    QCoreApplication::setApplicationName("QBear");

    QTranslator qtTranslator;

    QSettings settings;
    qtTranslator.load(":/lang/qbear_" + settings.value("locale/lang").toString());

    a.installTranslator(&qtTranslator);

    MainWindow w;
    w.show();
    
    return a.exec();
}
