#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <QtSql>
#include <QMessageBox>

namespace Ui {
class Login;
}

enum class logintable {
    LOGIN, PASSWORD, ID, ROLE
};

enum class roletable {
    CEO=1, DEALER
};

class Login : public QDialog
{
    Q_OBJECT
    
public:
    explicit Login(QWidget *parent = 0);
    ~Login();
    
private slots:
    void on_buttonBox_accepted();

signals:
    void connected();
    void isCEO(int id);
    void isDealer(int id);

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
